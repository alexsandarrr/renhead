<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\PaymentApprovalController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\TravelPaymentController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Public Routes
Route::post('/register', [AuthController::class, 'register']);
Route::post('/retrieve-token', [AuthController::class, 'retrieveToken']);

// Protected Routes
Route::group(['middleware' => ['auth:sanctum']], function () {
    // Payments Routes
    Route::get('/payments', [PaymentController::class, 'index']);
    Route::post('/payments/create', [PaymentController::class, 'create']);
    Route::put('/payments/update/{id}', [PaymentController::class, 'update']);
    Route::delete('/payments/delete/{id}', [PaymentController::class, 'delete']);

    // Travel Payments Routes
    Route::get('/travel-payments', [TravelPaymentController::class, 'index']);
    Route::post('/travel-payments/create', [TravelPaymentController::class, 'create']);
    Route::put('/travel-payments/update/{id}', [TravelPaymentController::class, 'update']);
    Route::delete('/travel-payments/delete/{id}', [TravelPaymentController::class, 'delete']);

    // Payment Approvals Routes
    Route::put('/payment-approvals/approve/{id}', [PaymentApprovalController::class, 'approve']);

    // Users Routes
    Route::put('/user/set-approver/{id}', [UserController::class, 'setApprover']);
    Route::put('/user/set-nonapprover/{id}', [UserController::class, 'setNonapprover']);

    // Reports Routes
    Route::get('/report', [ReportController::class, 'index']);
});
