<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Validator\UserValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;

class AuthController extends Controller
{
    public function retrieveToken (Request $request)
    {
        // Validating request data
        $fields = UserValidator::createTokenValidator($request);

        try {
            // Retreiving user
            $user = User::findOrFail($fields[User::FIELD_ID]);

            // Checking if user exists and if password matches
            if (!$user || !Hash::check($fields[User::FIELD_PASSWORD], $user->password))
            {
                return response([
                    'message' => 'Bad user id or password'
                ], 401);
            }

            // Creating token
            $token = $user->createToken('myapptoken')->plainTextToken;

            return response([
                'token' => $token
            ], 201);
        } catch (\Exception $e) {
            return response([
                'message' => 'There was an error creating token.'
            ]);
        }

    }
}
