<?php

namespace App\Http\Controllers;

use App\Models\PaymentApproval;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ReportController extends Controller
{
    public function index (Request $request)
    {
        $report = PaymentApproval::getReport();
        return response($report);
    }
}
