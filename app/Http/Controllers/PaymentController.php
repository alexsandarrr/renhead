<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Validator\PaymentValidator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PaymentController extends Controller
{

    public function index (Request $request)
    {
        $payments = Payment::all();
        return response($payments);
    }

    public function create (Request $request)
    {
        // Validating request data
        PaymentValidator::createValidator($request);

        try {
            // Creating new payment
            $payment = Payment::create($request->all());

            // Creating associated payment approval
            $payment->paymentApproval()->create($request->all());

            return response([
                'message' => 'Payment created successfully!'
            ], 201);

        } catch (\Exception $e) {
            return response([
                'message' => 'There was an error creating payment. Please check request parameters and try again.'
            ]);
        }
    }

    public function update (Request $request, int $id)
    {
        // Validating request data
        PaymentValidator::updateValidator($request);

        try {
            // Retrieving payment
            $payment = Payment::findOrFail($id);

            // Updating payment
            $payment->update($request->all());

            // Updating associated payment approval
            $payment->paymentApproval->update($request->all());

            return response([
                'message' => 'Payment updated successfully!'
            ]);
        } catch (\Exception $e) {
            return response([
                'message' => 'There was an error updating payment. Please check request parameters and try again.'
            ]);
        }
    }

    public function delete (Request $request, int $id)
    {
        try {
            // Retrieving payment
            $payment = Payment::findOrFail($id);

            // Deleting associated payment approval
            $payment->paymentApproval()->delete();

            // Deleting payment
            $payment->delete();

            return response([
                'message' => 'Payment deleted successfully!'
            ]);
        } catch (\Exception $e) {
            return response([
                'message' => 'There was an error deleting payment. Please check request parameters and try again.'
            ]);
        }
    }
}
