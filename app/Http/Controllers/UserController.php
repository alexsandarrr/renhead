<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    public function setApprover (Request $request, int $id)
    {
        try {
            $user = User::findOrFail($id);
            if (!$user->isApprover()) {
                $user->update([
                    'type' => User::APPROVER
                ]);

                return response([
                    'message' => 'User successfully set to APPROVER!'
                ]);
            }

            return response([
                'message' => 'User is already an  APPROVER!'
            ]);
        } catch (\Exception $e) {
            return response([
                'message' => 'There was an error while changing users type'
            ]);
        }
    }

    public function setNonapprover (Request $request, int $id)
    {
        try {
            $user = User::findOrFail($id);
            if ($user->isApprover()) {
                $user->update([
                    'type' => User::NONAPPROVER
                ]);

                return response([
                    'message' => 'User successfully set to NONAPPROVER!'
                ]);
            }

            return response([
                'message' => 'User is already an  NONAPPROVER!'
            ]);
        } catch (\Exception $e) {
            return response([
                'message' => 'There was an error while changing users type.'
            ]);
        }
    }
}
