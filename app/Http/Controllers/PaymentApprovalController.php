<?php

namespace App\Http\Controllers;

use App\Models\PaymentApproval;
use App\Models\Payment;
use App\Models\TravelPayment;
use App\Validator\PaymentApprovalValidator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PaymentApprovalController extends Controller
{
    public function approve (Request $request, int $id)
    {
        // Validating request data
        PaymentApprovalValidator::approveValidator($request);

        try {
            // Depending on requests parameter 'type' we are retrieving payment or travel payment
            $payment = $request->type === PaymentApproval::PAYMENT_TYPE ?
                Payment::findOrFail($id)
                : TravelPayment::findOrFail($id)
            ;

            // If this payment is not approved we are setting it's status col to APPROVED
            if (!$payment->isApproved())
            {
                $payment->paymentApproval->update($request->all());

                return response([
                    'message' => 'Payment approved successfully!'
                ]);
            }

            return response([
                'message' => 'This payment is already approved.'
            ]);
        } catch (\Exception $e) {
            return response([
                'message' => 'There was an error approving this payment.'
            ]);
        }
    }
}
