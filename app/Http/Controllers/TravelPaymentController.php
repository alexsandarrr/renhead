<?php

namespace App\Http\Controllers;

use App\Models\TravelPayment;
use App\Validator\TravelPaymentValidator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TravelPaymentController extends Controller
{
    public function index (Request $request)
    {
        $travelPayments = TravelPayment::all();
        return response($travelPayments);
    }

    public function create (Request $request)
    {
        // Validating request data
        TravelPaymentValidator::createValidator($request);

        try {
            // Creating travel payment
            $travelPayment = TravelPayment::create($request->all());

            // Creating associated payment approval
            $travelPayment->paymentApproval()->create($request->all());

            return response([
                'message' => 'Travel Payment created successfully!'
            ], 201);
        } catch (\Exception $e) {
            return response([
                'message' => 'There was an error creating travel payment. Please check request parameters and try again.'
            ]);
        }
    }

    public function update (Request $request, int $id)
    {
        // Validating request data
        TravelPaymentValidator::updateValidator($request);

        try {
            // Retrieving travel payment
            $travelPayment = TravelPayment::findOrFail($id);

            // Updating travel payment
            $travelPayment->update($request->all());

            // Updating associated payment approval
            $travelPayment->paymentApproval->update($request->all());

            return response([
                'message' => 'Payment updated successfully!'
            ]);
        } catch (\Exception $e) {
            return response([
                'message' => 'There was an error updating travel payment. Please check request parameters and try again.'
            ]);
        }
    }

    public function delete (Request $request, int $id)
    {
        try {
            // Retrieving payment
            $travelPayment = TravelPayment::findOrFail($id);

            // Deleting associated payment approval
            $travelPayment->paymentApproval()->delete();

            // Deleting payment
            $travelPayment->delete();

            return response([
                'message' => 'Travel Payment deleted successfully!'
            ]);
        } catch (\Exception $e) {
            return response([
                'message' => 'There was an error deleting travel payment. Please check request parameters and try again.'
            ]);
        }
    }
}
