<?php

namespace App\Validator;

use App\Models\TravelPayment;
use App\Models\User;
use Illuminate\Http\Request;

class TravelPaymentValidator
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public static function createValidator (Request $request): array
    {
        return $request->validate([
            TravelPayment::FIELD_USER_ID => 'required|integer|exists:users,id',
            TravelPayment::FIELD_AMOUNT  => 'required|numeric'
        ]);
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public static function updateValidator (Request $request): array
    {
        return $request->validate([
            TravelPayment::FIELD_USER_ID => 'integer|exists:users,id',
            TravelPayment::FIELD_AMOUNT  => 'numeric'
        ]);
    }
}
