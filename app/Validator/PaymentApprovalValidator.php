<?php

namespace App\Validator;

use App\Models\PaymentApproval;
use App\Models\User;
use Illuminate\Http\Request;

class PaymentApprovalValidator
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public static function approveValidator (Request $request): array
    {
        return $request->validate([
            PaymentApproval::TYPE         => 'string|in:' . implode(',', PaymentApproval::ALLOWED_TYPES),
            PaymentApproval::FIELD_STATUS => 'string|in:' . implode(',', PaymentApproval::ALLOWED_STATUSES)
        ]);
    }
}
