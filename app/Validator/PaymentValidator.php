<?php

namespace App\Validator;

use App\Models\Payment;
use App\Models\User;
use Illuminate\Http\Request;

class PaymentValidator
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public static function createValidator (Request $request): array
    {
        return $request->validate([
            Payment::FIELD_USER_ID      => 'required|integer|exists:users,id',
            Payment::FIELD_TOTAL_AMOUNT => 'required|numeric'
        ]);
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public static function updateValidator (Request $request): array
    {
        return $request->validate([
            Payment::FIELD_USER_ID      => 'integer|exists:users,id',
            Payment::FIELD_TOTAL_AMOUNT => 'numeric'
        ]);
    }
}
