<?php

namespace App\Validator;

use App\Models\User;
use Illuminate\Http\Request;

class UserValidator
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public static function createTokenValidator (Request $request): array
    {
        return $request->validate([
            User::FIELD_ID       => 'required|integer',
            User::FIELD_PASSWORD => 'required|string'
        ]);
    }
}
