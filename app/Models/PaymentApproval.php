<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class PaymentApproval extends Model
{
    use HasFactory, SoftDeletes;

    const APPROVED = 'APPROVED';
    const DISAPPROVED = 'DISAPPROVED';
    const ALLOWED_STATUSES = [
        self::APPROVED,
        self::DISAPPROVED
    ];

    const FIELD_STATUS       = 'status';

    const TYPE                = 'type';
    const PAYMENT_TYPE        = 'payment';
    const TRAVEL_PAYMENT_TYPE = 'travel_payment';
    const ALLOWED_TYPES = [
        self::PAYMENT_TYPE,
        self::TRAVEL_PAYMENT_TYPE,
    ];

    const REPORT_TYPE_PAYMENT_MODEL        = 'App\Models\Payment';
    const REPORT_TYPE_TRAVEL_PAYMENT_MODEL = 'App\Models\TravelPayment';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'payment_id',
        'payment_type',
        'status'
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return MorphTo
     */
    public function payments(): MorphTo
    {
        return $this->morphTo(__FUNCTION__, 'payment_type', 'payment_id');
    }

    /**
     * @return array
     */
    public static function getReport(): array
    {
        $paymentsRaw = self::leftJoin('users', 'payment_approvals.user_id', '=', 'users.id')
            ->selectRaw('payment_approvals.user_id, users.first_name, users.last_name, sum(payments.total_amount) AS payments_sum, sum(travel_payments.amount) AS travel_payments_sum')
            ->leftJoin('payments', function ($query) {
                $query->on('payment_approvals.payment_id', '=', 'payments.id')
                    ->where('payment_approvals.payment_type', '=', PaymentApproval::REPORT_TYPE_PAYMENT_MODEL)
                ;
            })
            ->leftJoin('travel_payments', function ($query) {
                $query->on('payment_approvals.payment_id', '=', 'travel_payments.id')
                    ->where('payment_approvals.payment_type', '=', PaymentApproval::REPORT_TYPE_TRAVEL_PAYMENT_MODEL)
                ;
            })
            ->where('payment_approvals.status', '=', PaymentApproval::APPROVED)
            ->where('users.type', '=', User::APPROVER)
            ->groupByRaw('payment_approvals.user_id, users.first_name, users.last_name')
            ->get();

        $payments = [];
        foreach ($paymentsRaw as $payment) {
            $payments[] = [
                'user_id' => $payment->user_id,
                'first_name' => $payment->first_name,
                'last_name' => $payment->last_name,
                'sum' => $payment->payments_sum + $payment->travel_payments_sum,
            ];
        }

        return $payments;
    }
}
