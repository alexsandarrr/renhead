<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Sanctum\HasApiTokens;

class User extends Model
{
    use HasApiTokens, HasFactory, SoftDeletes;

    const APPROVER      = 'APPROVER';
    const NONAPPROVER   = 'NONAPPROVER';

    const FIELD_ID         = 'id';
    const FIELD_PASSWORD   = 'password';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'type',
        'password'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password'
    ];

    /**
     * @return HasMany
     */
    public function payments(): HasMany
    {
        return $this->hasMany(Payment::class);
    }

    /**
     * @return HasMany
     */
    public function travelPayments(): HasMany
    {
        return $this->hasMany(TravelPayment::class);
    }

    /**
     * @return HasMany
     */
    public function paymentApprovals(): HasMany
    {
        return $this->hasMany(PaymentApproval::class);
    }

    /**
     * @return bool
     */
    public function isApprover (): bool
    {
        return $this->type === self::APPROVER;
    }
}
