<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Payment extends Model
{
    use HasFactory, SoftDeletes;

    const FIELD_USER_ID      = 'user_id';
    const FIELD_TOTAL_AMOUNT = 'total_amount';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'total_amount'
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return MorphOne
     */
    public function paymentApproval(): MorphOne
    {
        return $this->morphOne(PaymentApproval::class, 'payment');
    }

    /**
     * @return bool
     */
    public function isApproved (): bool
    {
        return $this->paymentApproval->status === PaymentApproval::APPROVED;
    }
}
