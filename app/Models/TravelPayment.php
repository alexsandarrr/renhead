<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class TravelPayment extends Model
{
    use HasFactory, SoftDeletes;

    const FIELD_USER_ID = 'user_id';
    const FIELD_AMOUNT  = 'amount';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'amount'
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'foreign_key');
    }

    /**
     * @return MorphOne
     */
    public function paymentApproval(): MorphOne
    {
        return $this->morphOne(PaymentApproval::class, 'payment');
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function isApproved (int $id): bool
    {
        $payment = self::findOrFail($id);
        return $payment->paymentApproval->status === PaymentApproval::APPROVED;
    }
}
